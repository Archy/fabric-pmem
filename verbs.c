#include <infiniband/verbs.h>
#include <rdma/rdma_cma.h>

#include <libpmemobj.h>

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <unistd.h>
#include <inttypes.h>
#include <errno.h>

// export LD_LIBRARY_PATH=/usr/local/lib:~/pmdk/src/nondebug
// gcc verbs.c -o vbs -I /root/pmdk/src/include/ -L ~/pmdk/src/nondebug -libverbs -lpmemobj -lrdmacm && ./vbs


////////////////////////////////////////////////////////////////////////
// FUNCTIONS
////////////////////////////////////////////////////////////////////////

/*
* Free resources
*/
void cleanup();

/**
 * Prints selected attributes of an RDMA device that is associated with a context
 */
void printDevAttrs();

/**
 * Prints selected attributes of a given port of an RDMA device context.
 */
void printPortsAttrs(uint8_t port_num);

/**
* Prints bit representation of 64bits integer
*/
void printBits(uint32_t x);

/*
 * Create pmem pool and register it in protected domain
 */
void createAndRegisterPool();

/*
 * Creates:
 *	- completion_channel
 *	- completion_queue
 *	- queue_pair
 */
void createQueuePair();

////////////////////////////////////////////////////////////////////////
// MACROS
////////////////////////////////////////////////////////////////////////
#define FAIL_SAVE(isOk, msg) \
	if (!isOk) \
	{ \
		printf("%s\n", msg); \
		cleanup(); \
		exit(1); \
	}

#define PMEM_FAIL_SAVE(isOk, msg) \
	if (!isOk){ \
		printf("%s\n", msg); \
		printf("%s\n", strerror(errno)); \
		cleanup(); \
		exit(1); \
	}

/*
 * Pmem layout name, used in create and open
 */
#define LAYOUT_NAME "intro_0"

/*
 * Maximum length of our buffer stored in pmem
 */
#define MAX_BUF_LEN 10

/*
 * The name of the memory pool file
 */
#define PATH "/root/jmajkutewicz/pverbs"

/*
 * Completion queue size
 */
#define CQ_SIZE 10

////////////////////////////////////////////////////////////////////////
// STRUCTS
////////////////////////////////////////////////////////////////////////

/*
 * Pmem root
 */
struct my_root {
	size_t len; /* = strlen(buf) */
	char buf[MAX_BUF_LEN];
};


////////////////////////////////////////////////////////////////////////
// GLOBALS
////////////////////////////////////////////////////////////////////////

//RDMA CM
struct rdma_event_channel *cm_channel = NULL;
struct rdma_cm_id *cm_id = NULL;
struct rdma_cm_event *event = NULL;
struct rdma_conn_param conn_param = {};

//VERBS
struct ibv_context *dev_context = NULL;
struct ibv_pd *protection_domain = NULL;
struct ibv_mr *memory_region = NULL;

struct ibv_comp_channel *completion_channel = NULL;
struct ibv_cq *completion_queue = NULL;
struct ibv_qp *queue_pair = NULL;

//PMEM
PMEMobjpool *pmem_pool = NULL;
PMEMoid pmem_root;
struct my_root *direct_root = NULL;

//SHARED
char *buffer = NULL;


int main(int argc, char *argv[])
{
	int result;
	int isMaster;
	if (argc == 0)
	{
		return 1;
	}
	printf("%s\n", argv[1]);
	isMaster = argv[1][0] == 'm';

	/* Set up RDMA CM structures */
	cm_channel = rdma_create_event_channel();
	FAIL_SAVE(cm_channel != NULL, "Creating cm_channel failed");

	struct ibv_device **dev_list = ibv_get_device_list(NULL);

	//list devices
	struct ibv_device **dev_it = dev_list;
	while((*dev_it) != NULL)
	{
		printf("%s\t%s\t" PRIu64 "\n", 
				ibv_get_device_name(*dev_it),
				ibv_node_type_str((*dev_it)->node_type),
				ibv_get_device_guid(*dev_it));

		dev_it++;
	}
	//open device
	dev_context = ibv_open_device(*dev_list);
	FAIL_SAVE(dev_context != NULL, "Opening device failed");
	ibv_free_device_list(dev_list);
	printf("Device opened\n");
	
	//https://github.com/linux-rdma/rdma-core/blob/master/libibverbs/verbs.h
	printDevAttrs();
	//first port nr is 1!
	printPortsAttrs(1);
	printPortsAttrs(2);

	//Create a Protection Domain
	protection_domain = ibv_alloc_pd(dev_context);
	FAIL_SAVE(protection_domain != NULL, "Creating protection domain failed");
	printf("Protection domain created\n");

	createAndRegisterPool();

	createQueuePair();

	//print stuff for QP connection
	struct ibv_qp_attr qp_attr;
	struct ibv_qp_init_attr qp_init_attr;
	result = ibv_query_qp(queue_pair,
		&qp_attr,
		~((int)0),		//force fill all
		&qp_init_attr);
	FAIL_SAVE(result == 0, "Quering qp failed");

	//TODO PKEY
	if (isMaster)
	{
		//Y
		printf("\tsq_psn: %u\n", qp_attr.sq_psn);
		printf("\tqp_num: %d\n", queue_pair->qp_num);
		printf("\tpath_mtu: %d\n", qp_attr.path_mtu);

		printf("\tmax_dest_rd_atomic: %d\n", qp_attr.max_dest_rd_atomic);
		printf("\tqp_attr.port_num: %d\n", (int)qp_attr.port_num);
		printf("\teq: %d\n", (int)qp_attr.ah_attr.dlid);
	}
	else
	{
		//X
		printf("\trq_psn: %u\n", qp_attr.rq_psn);
		printf("\tdest_qp_num: %d\n", qp_attr.dest_qp_num);
		printf("\tpath_mtu: %d\n", qp_attr.path_mtu);

		printf("\tmax_rd_atomic: %d\n", qp_attr.max_rd_atomic);
		printf("\tport_num: %d\n", (int)qp_attr.port_num);
		printf("\tah_attr.port_num: %d\n", (int)qp_attr.ah_attr.port_num);
		printf("\tqp_attr.ah_attr.dlid: %d\n", (int)qp_attr.ah_attr.dlid);
		printf("\tsum: %d\n", (int)qp_attr.port_num + qp_attr.ah_attr.src_path_bits);
	}

	//Scatter/Gather (S/G) elements
	/*
	 * Every Work Request contains usually one or more S/G entries:
	 *	- Every S/G entry refers to a Memory Region or part of it
	 *	- No S/G entries means zero-byte message
	 *	- Gather � when local data is read and sent over the wire
	 *	- Scatter � when data is received and written locally
	 */

	// Post Send Request
	/*
	 * Add a Send Request to the Send Queue (No context switch will occur ^^)
	 *
	 * Specify the attributes of the data transfer:
	 *	- How data will be sent (opcode, attributes)
	 *	- How much data will be sent
	 *	- Which local memory buffer(s) to read/write to
	 *	- If RDMA: the remote memory buffer attributes
	 * 
	 * Every Send Request is considered outstanding until a work Completion was generated
	 * for it or for other Send Request that followed it
	 */
	struct ibv_send_wr *bad_send_wr;
	struct ibv_send_wr send_wr = {};
//	ibv_post_send(
//		queue_pair, 
//		&send_wr,		// a linked list of Send Requests
//		&bad_send_wr	// will be assigned 
//	);

	//Post Receive Request
	/*
	*
	*
	*/

	//Polling for Work Completion
	/*
	 * 
	 *
	 */

	printf("Done\n");
	cleanup();
	return 0;
}

void cleanup()
{
	//VERBS
	//Destroy a Queue Pair
	//  should be called after detach is from all multicast groups
	if (queue_pair != NULL)
	{
		ibv_destroy_qp(queue_pair);
	}
	//Destroy a Completion Queue
	//  should be called after destroying all the QPs that are associated with it
	if (completion_queue != NULL)
	{
		ibv_destroy_cq(completion_queue);
	}
	//Destroy a Completion Event channel
	//	should be called after destroying all the CQs that are associated with it
	if (completion_channel != NULL)
	{
		ibv_destroy_comp_channel(completion_channel);
	}
	//Deregister Memory Region
	if (memory_region != NULL)
	{
		ibv_dereg_mr(memory_region);
	}
	//Destroy a Protection Domain; should be called after destroying 
	//	all the resources that are associated with it
	if(protection_domain != NULL)
	{
		ibv_dealloc_pd(protection_domain);
	}
	//close context of the RDMA device
	if(dev_context != NULL)
	{
		ibv_close_device(dev_context);
	}

	//PMEM
	if (pmem_pool != NULL)
	{
		pmemobj_close(pmem_pool);
	}

	//RDMA_CM
	// Close an event communication channel.
	if(cm_channel != NULL)
	{
		rdma_destroy_event_channel(cm_channel);
	}
}

void printDevAttrs()
{
	//print device attributes
	//http://www.rdmamojo.com/2012/07/13/ibv_query_device/

	struct ibv_device_attr device_attr;
	int result = ibv_query_device(dev_context, &device_attr);
	FAIL_SAVE(result == 0, "Quering dev attrs failed");
	printf("Attr: max_mr_size: %llu \n\tpage_size_cap: %llu \n\tmax_qp_wr: %d \n\tatomic_cap: %d \n\tphys_port_cnt: %u\n",
		device_attr.max_mr_size,
		device_attr.page_size_cap,
		device_attr.max_qp_wr,
		device_attr.atomic_cap,
		(unsigned int)device_attr.phys_port_cnt);
}

void printPortsAttrs(uint8_t port_num)
{
	//print ports attr
	//http://www.rdmamojo.com/2012/07/21/ibv_query_port/

	struct ibv_port_attr port_attr;
	int result = ibv_query_port(dev_context, port_num, &port_attr);
	FAIL_SAVE(result == 0, "Quering port attrs failed");
	printf("Port attr:\n\t state: %d\n\t phys_state: %u\n\t link_layer: %u\n\t port_cap_flags: ", 
			port_attr.state, 
			(unsigned int)port_attr.phys_state,
			(unsigned int)port_attr.link_layer);
	printBits(port_attr.port_cap_flags);
	printf("\n");

	/*
	 *	State: The logical port state
	 *		1 - down
	 *		2 - Initializing
	 *		3 - armed (The physical link of the port is up, but the SM haven't yet fully configured the logical link)
	 *		4 - active
	 *
	 *	Phys state: physical link status
	 *		1 - Sleep
	 *		2 - Polling
	 *		3 - Disabled
	 *		4 - PortConfigurationTraining
	 *		5 - LinkUp
	 *
	 *	link_layer:
	 *		0 - InfiniBand
	 *		1 - InfiniBand
	 *		2 - Ethernet, thus IBoE (or RoCE) can be used
	 */
}

void printBits(uint32_t v)
{
	uint32_t mask = 1u << 31;
	int i;
	for (i = 0; i<32; ++i)
	{
		printf("%d", (v&mask ? 1 : 0));
		mask >>= 1;
	}
}

void createAndRegisterPool()
{
	//Create pool
	if (access(PATH, F_OK) != -1) {
		// file exists
		pmem_pool = pmemobj_open(PATH, LAYOUT_NAME);
	}
	else {
		// file doesn't exist
		pmem_pool = pmemobj_create(PATH, LAYOUT_NAME, PMEMOBJ_MIN_POOL, 0666);
	}
	PMEM_FAIL_SAVE(pmem_pool != NULL, "Creating or opening pool failed");
	printf("Memory pool created\n");
	// request the root object
	pmem_root = pmemobj_root(pmem_pool, sizeof(struct my_root));
//	FAIL_SAVE(pmem_root != OID_NULL, "Acquiring root object failed");
	// translate it to a usable, direct pointer
	direct_root = (struct my_root *)pmemobj_direct(pmem_root);
	PMEM_FAIL_SAVE(direct_root != NULL, "Translating root obj failed");
	buffer = direct_root->buf;
	printf("Pool acquired\n");

	//Register memory region
	/*
	* Memory Region is a virtually contiguous memory block that was registered,
	*	i.e.prepared for work with RDMA.
	*
	* Any memory buffer in the process� virtual space can be registered
	*
	* One must specify permission to the region. If Remote Write or Remote Atomic is enabled,
	*	local Write should be enabled too
	*
	* The same memory buffer can be registered multiple times
	*
	* After a successful memory registration, two keys are being generated:
	*	- Local Key (lkey)
	*	- Remote Key (rkey)
	* Those keys are used when referring to this MR in a Work Request
	*/
	memory_region = ibv_reg_mr(protection_domain, buffer, MAX_BUF_LEN,
		IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_REMOTE_READ);
	FAIL_SAVE(memory_region != NULL, "Creating protection domain failed");
	printf("Memory region registered\n");
}

void createQueuePair()
{
	//Create a new Completion Event channel
	/*
	* Completion Event channel is a mechanism for delivering notification about the creation
	* of Work Completions in CQs that is attached to it
	*
	* This object will be used when creating new CQs
	*
	* One Completion Event channel can be used with multiple CQs
	*/
	completion_channel = ibv_create_comp_channel(dev_context);
	FAIL_SAVE(completion_channel != NULL, "Creating completion event channel failed");
	printf("Completion event channel created\n");

	//Create a new Completion Queue
	/*
	* Completion Queue is a Queue that holds information about completed Work Requests
	*
	* A Completion Queue size is limited - overruled WR are moved to the Error state
	*	- int ibv_resize_cq(struct ibv_cq *cq, int cqe);
	*
	* One CQ can be shared with multiple queues: Queue Paris, Send Queues, Receive Queues, Mix of all
	*/
	completion_queue = ibv_create_cq(
		dev_context,
		CQ_SIZE,				//The minimum requested capacity of the CQ
		NULL,					//(optional) User defined value which will be available in cq->cq_context
		completion_channel,		//(optional!!!) The Completion event channel that will be used to indicate that new Work Completion was added to this CQ. 
		0);						//???(0 in example) http://www.rdmamojo.com/2012/11/03/ibv_create_cq/
	FAIL_SAVE(completion_queue != NULL, "Creating completion queue failed");
	printf("Completion queue created\n");

	//Create a new Queue Pair
	/*
	* Queue Pair is the actual object that transfers data. It encapsulates both Send and Receive Queue. Full duplex
	*
	* There are three major transport types:
	*	- Reliable Connected (RC): An RC QP is connected to a single RC QP. Supports operations that need ACK
	*	- Unreliable Connected (UC): An UC QP connected to a single UC QP; Does not support RDMA Read
	*	- Unreliable Datagram (UD): An UD QP can send/receive messages to/from any UD QP; Does not support RDMA Write/Read
	*								Reliability is not guaranteed; Multicast is supported;
	*/
	/*
	* Connecting QPs: communication should be established between the connected QPs:
	*	- Each side needs to know who is the other side
	*	- Each side needs to have information about the other side and the path to it
	*	- Each side needs to configure attributes that describe the send attributes
	*
	* Solutions:
	*	- Exchange information Out Of Band  (eg. sockets)
	*	- Use Communication Manager (CM) (THIS IS THE PROPER WAY!)
	*
	* Information needs to be exchanged: QP nr, LID nr, RQ Packet Serial Number (PSN), GID
	* Path MTU must be equal on both sides
	* If RDMA opcodes are used, the permissions of QP and MR should be configured to support them
	*
	* In each QP state transition, the relevant attributes to enable the state functionality needs to be configured
	*/
	struct ibv_qp_init_attr init_attr = {
		.send_cq = completion_queue,		// The CQ to be associated with its Send Queue
		.recv_cq = completion_queue,		// The CQ to be associated with its Receive Queue
		.cap = {							// The QP attributes to be created
			.max_send_wr = 1,					// The number of Send Requests that can be outstanding in the QP
			.max_recv_wr = 1,					// The number of Receive Requests that can be outstanding in the QP
			.max_send_sge = 1,					// The number of S/G entries that each Send Request may hold
			.max_recv_sge = 1					// The number of S/G entries that each Receive Request may hold
		},
		.qp_type = IBV_QPT_RC				// The QP transport type
	};
	queue_pair = ibv_create_qp(protection_domain, &init_attr);
	FAIL_SAVE(queue_pair != NULL, "Creating queue pair failed");
	printf("queue pair created\n");
}