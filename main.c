#include <rdma/fabric.h>
#include <rdma/fi_domain.h>
#include <rdma/fi_endpoint.h>
#include <rdma/fi_cm.h>
#include <rdma/fi_tagged.h>
#include <rdma/fi_rma.h>
#include <rdma/fi_errno.h>

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

/**
 *	libfabric controling log with env vars:
 *	- log level:			export FI_LOG_LEVEL="Debug"
 *	- choosing subsystems:	export FI_LOG_SUBSYS="core,fabric,domain" 
 *	https://ofiwg.github.io/libfabric/v1.2.0/man/fabric.7.html
 */

//fabric:
struct fid_fabric *fabric = NULL;
struct fid_eq *eq = NULL;
struct fid_domain *domain = NULL;
//lane:
struct fid_ep *ep = NULL;
struct fid_cq *cq = NULL;

/*
 * Returns provider to use
 */
struct fi_info* getProvider();

/*
 * Free resources
 */
void cleanup();

/**
 * Prints bit representation of 64bits integer
 */
void printBits(uint64_t x);

#define FAIL_SAVE(err, msg) \
	if (err != 0){ \
		printf("%s:\t%s\n", msg, fi_strerror(-err)); \
		cleanup(); \
		exit(err); \
	}
	
	

int main(int argc, char **argv)
{
	int err;
	printf("Starting\n");
	struct fi_info *fi = getProvider();

//	FAIL_SAVE(1, "Failsave test\n");

	//create fabric	
	err = fi_fabric(
		fi->fabric_attr, 
		&fabric, 
		NULL);	//User specified context associated with the opened object
	FAIL_SAVE(err, "Fabric creation failed");
	printf("Fabric created\n");

	//create domain object	-	usually map to a specific local network interface adapter
	err = fi_domain(fabric, fi, &domain, NULL);
	FAIL_SAVE(err, "Domain creation failed");
	printf("Domain created\n");

	//create queue
	struct fi_eq_attr eq_attr;
	memset(&eq_attr, 0, sizeof(eq_attr));
	eq_attr.size = 64;
	eq_attr.wait_obj = FI_WAIT_UNSPEC;
	err = fi_eq_open(fabric, &eq_attr, &eq, NULL);
	FAIL_SAVE(err, "Queue creation failed");
	printf("Queue created\n");

	//create endpoint - object used for communication
	err = fi_endpoint(domain, fi, &ep, NULL);
	FAIL_SAVE(err, "Endpoint creation failed");
	printf("Endpoint created\n");

	//TODO create address vector???

	//create copletion queue
	struct fi_cq_attr cq_attr;
	memset(&cq_attr, 0, sizeof(cq_attr));
	cq_attr.format = FI_CQ_FORMAT_UNSPEC;	//https://ofiwg.github.io/libfabric/v1.0.0/man/fi_cq.3.html
	cq_attr.size = 100;
	err = fi_cq_open(domain, &cq_attr, &cq, NULL);
	FAIL_SAVE(err, "Completion queue creation failed");
	printf("Completion queue created\n");

	//bind cq to endpoint
	err = fi_ep_bind(ep, (fid_t)cq, FI_READ | FI_WRITE | FI_REMOTE_READ | FI_REMOTE_WRITE);
	FAIL_SAVE(err, "CQ binding failed");
	printf("CQ binded\n");

	//create memory region
	// RMA and atomic operations can both read and write memory that is owned by a peer process, 
	// and neither require the involvement of the target processor.Because the memory can be modified over the network, 
	// an application must opt into exposing its memory to peers.This is handled by the memory registration process.

	//cleanup
	cleanup();

	return 0;
}

void cleanup()
{

	//release all resources associated with a fabric domain or interface
	if (cq != NULL)
	{
		fi_close(&(cq->fid));
	}
	if (ep != NULL)
	{
		fi_close(&(ep->fid));
	}
	//domain
	if (domain != NULL)
	{
		fi_close(&(domain->fid));
	}
	//queue
	if (eq != NULL)
	{
		fi_close(&(eq->fid));
	}
	//fabric
	if (fabric != NULL)
	{
		fi_close(&(fabric->fid));
	}
}

void printBits(uint64_t v)
{
	uint64_t mask = 1ULL<<63;
	int i;
	for(i=0; i<64; ++i)
	{
		printf("%d", (v&mask ? 1 : 0));
		mask >>= 1;
	}
}

struct fi_info* getProvider()
{
	printBits(FI_RMA);
	printf("\n");

	int err;
	struct fi_info *fi, *hints = NULL;
	// hints allows filter out providers
	hints = fi_allocinfo();						// recomended, since hints param has a non-flat memory layout
//	hints->fabric_attr = NULL;
//	hints->ep_attr->type = FI_EP_MSG; // FI_EP_RDM;			// Optionally supplied endpoint attributes; here: reliable, unconnected
//	hints->caps = FI_RMA | FI_MSG | FI_TAGGED;			// indicates the desired capabilities of the fabric interfaces;
//	hints->mode = FI_LOCAL_MR | FI_RX_CQ_DATA;				//req placed on app; here: 
												//	FI_LOCAL_MR - provider requires that locally accessed data buffers be registered with provider before being used. Supports existing IB hardware		
												//	FI_RX_CQ_DATA - transfer which carry remote CQ data consume receive buffer space; Supports IB
	//	hints->fabric_attr->prov_name = strdup("verbs");

	//discover available providers
	err = fi_getinfo(
		FI_VERSION(1, 6),
		NULL,	// Optional, name or fabric address to resolve
		NULL,	// Optional, service name or port number of address.
		0,		// Operation flags for the fi_getinfo call.
		NULL,	//hints	// Reference to an fi_info structure that specifies criteria for selecting the returned fabric information.
		&fi);
	if (err != 0)
	{
		printf("Providers discovery failed\n");
		fi_freeinfo(hints);
		exit(1);
	} 

	//print all found providers
	struct fi_info *fi_it = fi;
	while (fi_it != NULL)
	{
		printf("Provider:\t%s \t%s\t %d\t%d\t",
			fi_it->fabric_attr->name,
			fi_it->fabric_attr->prov_name,
			fi_it->fabric_attr->prov_version,
			fi_it->fabric_attr->api_version);
		//capabilities bit mask, itoa is not supported
		printBits(fi_it->mode);
		printf("\n");
		printBits(fi_it->caps);
		printf("\t%d\n", fi_it->addr_format);

		fi_it = fi_it->next;
	}
	//Test: take third one:
//	fi = fi->next->next;

	fi_freeinfo(hints);
	return fi;
}

//FI_RMA_PMEM:
//	  1111111111111001111111111111111111111111111111111111111111111110
//					10000000000000000000000000000000000000000000000000

// 1111111111111001111111111111111111111111111111111111111111111110
// 1111111111111000000011111111111111111111111111111111111111111110