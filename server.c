/*
* build:
*   cc -o server server.c -libverbs -lrdmacm
*   cc -o server server.c -I /root/pmdk/src/include/ -L ~/pmdk/src/nondebug -libverbs -lpmemobj -lrdmacm
*
* usage:
*   server
*
* waits for client to connect, receives two integers, and sends their
* sum back to the client.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <infiniband/arch.h>
#include <rdma/rdma_cma.h>

#include <libpmemobj.h>


#define PATH "/root/jmajkutewicz/rdma_verbs"

#define LAYOUT_NAME "rdma"

/*
* Pmem root
*/
struct my_root {
	size_t len; /* = strlen(buf) */
	char buf[10];
};

enum {
	RESOLVE_TIMEOUT_MS = 5000,
	BUF_SIZE = 10,
};

struct pdata {
	uint64_t	buf_va;
	uint32_t	buf_rkey;
};

//PMEM
PMEMobjpool *pmem_pool = NULL;
PMEMoid pmem_root;
struct my_root *direct_root = NULL;



int main(int argc, char *argv[])
{
	struct pdata			rep_pdata;

	struct rdma_event_channel      *cm_channel;
	struct rdma_cm_id	       *listen_id;
	struct rdma_cm_id	       *cm_id;
	struct rdma_cm_event	       *event;
	struct rdma_conn_param		conn_param = {};

	struct ibv_pd		       *pd;
	struct ibv_comp_channel	       *comp_chan;
	struct ibv_cq		       *cq;
	struct ibv_cq		       *evt_cq;
	struct ibv_mr		       *mr;
	struct ibv_qp_init_attr		qp_attr = {};
	struct ibv_sge			sge;
	struct ibv_send_wr		send_wr = {};
	struct ibv_send_wr	       *bad_send_wr;
	struct ibv_recv_wr		recv_wr = {};
	struct ibv_recv_wr	       *bad_recv_wr;
	struct ibv_wc			wc;
	void			       *cq_context;

	struct sockaddr_in		sin;

	char		       *buf;

	int				err;

	/* Set up RDMA CM structures */

	cm_channel = rdma_create_event_channel();
	if (!cm_channel)
		return 1;

	err = rdma_create_id(cm_channel, &listen_id, NULL, RDMA_PS_TCP);
	if (err)
		return err;

	sin.sin_family = AF_INET;
	sin.sin_port = htons(20079);
	sin.sin_addr.s_addr = INADDR_ANY;

	/* Bind to local port and listen for connection request */

	err = rdma_bind_addr(listen_id, (struct sockaddr *) &sin);
	if (err)
		return 1;

	err = rdma_listen(listen_id, 1);
	if (err)
		return 1;

	err = rdma_get_cm_event(cm_channel, &event);
	if (err)
		return err;

	if (event->event != RDMA_CM_EVENT_CONNECT_REQUEST)
		return 1;

	cm_id = event->id;

	rdma_ack_cm_event(event);

	/* Create verbs objects now that we know which device to use */

	pd = ibv_alloc_pd(cm_id->verbs);
	if (!pd)
		return 1;

	comp_chan = ibv_create_comp_channel(cm_id->verbs);
	if (!comp_chan)
		return 1;

	cq = ibv_create_cq(cm_id->verbs, 2, NULL, comp_chan, 0);
	if (!cq)
		return 1;

	if (ibv_req_notify_cq(cq, 0))
		return 1;

	buf = calloc(BUF_SIZE, sizeof(char));
	if (!buf)
		return 1;

	printf("Creating pool\n");
	//Create pool
	if (access(PATH, F_OK) != -1) {
		// file exists
		pmem_pool = pmemobj_open(PATH, LAYOUT_NAME);
	}
	else {
		// file doesn't exist
		pmem_pool = pmemobj_create(PATH, LAYOUT_NAME, PMEMOBJ_MIN_POOL, 0666);
	}
	if(pmem_pool == NULL) 
		return 1;
	printf("Memory pool created\n");
	// request the root object
	pmem_root = pmemobj_root(pmem_pool, sizeof(struct my_root));
	// translate it to a usable, direct pointer
	direct_root = (struct my_root *)pmemobj_direct(pmem_root);
	if (direct_root == NULL)
		return 1;
	buf = direct_root->buf;
	printf("Pool acquired\n");

	//Register memory region
	mr = ibv_reg_mr(pd, buf, BUF_SIZE,
		IBV_ACCESS_LOCAL_WRITE |
		IBV_ACCESS_REMOTE_READ |
		IBV_ACCESS_REMOTE_WRITE);
	if (!mr)
		return 1;
	printf("Memory region created");

	qp_attr.cap.max_send_wr = 1;
	qp_attr.cap.max_send_sge = 1;
	qp_attr.cap.max_recv_wr = 1;
	qp_attr.cap.max_recv_sge = 1;

	qp_attr.send_cq = cq;
	qp_attr.recv_cq = cq;

	qp_attr.qp_type = IBV_QPT_RC;

	err = rdma_create_qp(cm_id, pd, &qp_attr);
	if (err)
		return err;

	/* Post receive before accepting connection */

	printf("Posting recv\n");

	sge.addr = buf;
	sge.length = BUF_SIZE;
	sge.lkey = mr->lkey;

	recv_wr.sg_list = &sge;
	recv_wr.num_sge = 1;

	if (ibv_post_recv(cm_id->qp, &recv_wr, &bad_recv_wr))
		return 1;

	rep_pdata.buf_va = htonll((uintptr_t)buf);
	rep_pdata.buf_rkey = htonl(mr->rkey);

	conn_param.responder_resources = 1;
	conn_param.private_data = &rep_pdata;
	conn_param.private_data_len = sizeof rep_pdata;

	/* Accept connection */

	printf("Accepting connections\n");

	err = rdma_accept(cm_id, &conn_param);
	if (err)
		return 1;

	err = rdma_get_cm_event(cm_channel, &event);
	if (err)
		return err;

	if (event->event != RDMA_CM_EVENT_ESTABLISHED)
		return 1;

	rdma_ack_cm_event(event);

	/* Wait for receive completion */

	printf("Waiting for receive completion\n");

	int x;
	scanf("%d", &x);

	printf("BUF: %s\n", buf);
	pmemobj_close(pmem_pool);

	if (ibv_get_cq_event(comp_chan, &evt_cq, &cq_context))
		return 1;


	if (ibv_req_notify_cq(cq, 0))
		return 1;

	if (ibv_poll_cq(cq, 1, &wc) < 1)
		return 1;

	if (wc.status != IBV_WC_SUCCESS)
		return 1;

	ibv_ack_cq_events(cq, 1);

	

	return 0;
}