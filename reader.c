#include <stdio.h>
#include <string.h>
#include <libpmemobj.h>

//cc -o reader reader.c -I /root/pmdk/src/include/ -L ~/pmdk/src/nondebug -lpmemobj

#define PATH "/root/jmajkutewicz/rdma_verbs"

#define LAYOUT_NAME "rdma"

/*
* Pmem root
*/
struct my_root {
	size_t len; /* = strlen(buf) */
	char buf[10];
};

PMEMobjpool *pmem_pool = NULL;
PMEMoid pmem_root;
struct my_root *direct_root = NULL;

int main(int argc, char *argv[])
{
	pmem_pool = pmemobj_open(PATH, LAYOUT_NAME);
	pmem_root = pmemobj_root(pmem_pool, sizeof(struct my_root));
	direct_root = (struct my_root *)pmemobj_direct(pmem_root);
	char *buf = buf = direct_root->buf;

	printf("%s\n", buf);


	// release the pool
	pmemobj_close(pmem_pool);
	return 0;
}