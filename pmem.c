#include <stdio.h>
#include <string.h>
#include <libpmemobj.h>
#include "Layout.h"

void read_string(PMEMobjpool *pop, struct my_root *rootp)
{
	// read string to save
	char buf[MAX_BUF_LEN];
	scanf("%9s", buf);

	// persists the data
	//	The _persist suffixed functions make sure that the range of memory they operate on is flushed
	//	store length for validating string integrity
	//	pmemobj_persist - pmemobj version of pmem_persist; takes:
	//				* PMEMobjpool* pool
	//				* const void *add
	//				* size_t len
	rootp->len = strlen(buf);
	pmemobj_persist(pop, &rootp->len, sizeof(rootp->len));
	pmemobj_memcpy_persist(pop, rootp->buf, buf, rootp->len);
}

void transactional_read_string(PMEMobjpool *pop, PMEMoid root, struct my_root *rootp)
{
	const int size = 20;
	// volatile:
	//	Local non-volatile qualified objects have undefined values 
	//		after execution of longjmp if their value have changed after setjmp.
	//	Every local variable modified in TX_STAGE_WORK and used in 
	//		TX_STAGE_ONABORT/TX_STAGE_FINALLY needs to be volatile-qualified
	volatile char *buff = NULL;

	// TRANSACTIONS
	TX_BEGIN(pop) {	//REQUIRED
		/* TX_STAGE_WORK */
		buff = (char*) malloc(size * sizeof(char));
		if (buff == NULL)
			pmemobj_tx_abort(ENOMEM);

		scanf("%20s", buff);
		int len = 0;
		while (buff[len] != NULL && len < 20)
		{
			len++;
		}

		pmemobj_tx_add_range(root, 0, sizeof(struct my_root));
		rootp->len = len;
		memcpy(rootp->buf, (char*)buff, len);

		//also valid, without using PMEMoid
//		pmemobj_tx_add_range_direct(rootp, sizeof(struct my_root));
//		rootp->len = strlen(buff);
//		memcpy(rootp->buf, buff, strlen(buff));
	} TX_ONCOMMIT{	//OPTIONAL
		/* TX_STAGE_ONCOMMIT */
		//called when the transaction commits
		//is not rolled back on abort!
	} TX_ONABORT{	//OPTIONAL
		/* TX_STAGE_ONABORT */
		//called when the transaction aborts
	} TX_FINALLY{	//OPTIONAL
		/* TX_STAGE_FINALLY */
		// guarented to run
		free((void*)buff);
	} TX_END		//REQUIRED

	//you are not guaranteed that the code here will be executed (due to longjmp on all aborts)
	//free(buff);
}

int main(int argc, char *argv[])
{
	const char* path = "/root/jmajkutewicz/pfile1";

	// path:		path to file
	// layout:		string that identifies the pool
	// poolsize:	PMEMOBJ_MIN_POOL = 8MiB
	// mode:		standard file mode
	PMEMobjpool *pop = pmemobj_create(path, LAYOUT_NAME, PMEMOBJ_MIN_POOL, 0666);
	if (pop == NULL) {
		perror("pmemobj_create");
		return 1;
	}

	// request the root object
	//	PMEMoid - persistent memory pointer; consists of:
	//				* pool_uuid_lo - unique id of the pool
	//				* off - the offset from the start of the pool (not the VAS)
	//	pmemobj_root - returns the root object; takes:
	//				* PMEMobjpool pool
	//				* size - the size of the structure you want as root object
	PMEMoid root = pmemobj_root(pop, sizeof(struct my_root));
	// translate it to a usable, direct pointer.
	//	to get the DIRECT pointer, a simple addition can be performed 
	//		(if you know the virtual address the pool is mapped at):
	//		direct = (void *)((uint64_t)pool + oid.off)
	//	pmemobj_direct - computes the direct pointer, takes:
	//				* PMEMoid - persistent pointer
	//		The pool id is used to figure out where the pool is currently mapped
	//		All open pools are stored in a cuckoo hash table with 2 hashing functions, 
	//		which is used to locate the pool address.
	struct my_root *rootp = (struct my_root *)pmemobj_direct(root);

//	read_string(pop, rootp);
	transactional_read_string(pop, root, rootp);


	// release the pool
	pmemobj_close(pop);
	return 0;
}